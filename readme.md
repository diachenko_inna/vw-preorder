## Volkswagen ID.3 Preorder Form

####Deploy project

1. `npm install`
2. `composer install`
3. `cp .env.example .env`
4. `php artisan storage:link`
5. `chmod –R  777 ./storage ./bootstrap/cache`
6. Create MySql Database 
    <br>**name:** vw-preorder
    <br>**user**: vw-preorder 
    <br>**password**: vw-preorder
7. `php artisan migrate`
8. `php artisan queue:restart`

_Server Requirements_ you can found [there](https://laravel.com/docs/6.x#server-requirements).<br>
_Server Configuration_ you can found [there](https://laravel.com/docs/6.x#web-server-configuration).
