<?php

namespace App\Events;

use App\Models\Preorder;
use Illuminate\Queue\SerializesModels;

class PreorderCreated
{
    use SerializesModels;

    /**
     * The new order confirmation.
     *
     * @var Preorder $preorder
     */
    public $preorder;

    /**
     * Create a new event instance.
     *
     * @param  Preorder $preorder
     * @return void
     */
    public function __construct($preorder)
    {
        $this->preorder = $preorder;
    }
}
