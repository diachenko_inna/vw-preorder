<?php

namespace App\Console\Commands;

use App\Models\Preorder;
use App\Notifications\NewPreorders;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class SendNewPreordersNotifications extends Command
{
    /** @var string $notificationChannel */
    private $notificationChannel = 'mail';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-notifications:send-new-preorder-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification about all created preorders for last hour';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     **/
    public function handle()
    {
        $datetime = Carbon::now()->subHour()->format("Y-m-d H");
        $preorders = Preorder::where('created_at', 'like', $datetime."%")->get();
        $email = config('app.holder', 'admin@example.com');

        Notification::route($this->notificationChannel, $email)
            ->notify(new NewPreorders($preorders));
    }
}
