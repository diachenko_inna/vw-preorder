<?php

namespace App\Jobs;

use App\Models\Preorder;
use App\Notifications\PreorderConfirmation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

class SendPreorderConfirmationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var string $notificationChannel */
    private $notificationChannel = 'mail';

    /** @var Preorder $preorder */
    private $preorder;

    /**
     * Create a new job instance.
     *
     * @param Preorder $preorder
     * @return void
     */
    public function __construct(Preorder $preorder)
    {
        $this->preorder = $preorder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Notification::route($this->notificationChannel, $this->preorder->email)
            ->notify(new PreorderConfirmation($this->preorder));
    }
}
