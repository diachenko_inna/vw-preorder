<?php

namespace App\Listeners;

use App\Events\PreorderCreated;
use App\Jobs\SendPreorderConfirmationEmail;

class SendPreorderConfirmationNotification
{
    /**
     * Handle the preorder created event.
     * Add send preorder confirmation email to queue
     *
     * @param  \App\Events\PreorderCreated $event
     * @return void
     */
    public function handle(PreorderCreated $event)
    {
        dispatch(new SendPreorderConfirmationEmail($event->preorder));
    }
}
