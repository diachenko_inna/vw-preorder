<?php

namespace App\Http\Controllers;

use App\Events\PreorderCreated;
use App\Models\Preorder;
use Illuminate\Http\Request;

class PreorderController extends Controller
{
    /**
     * Create new preorder
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function store(Request $request)
    {
        $request->validate(Preorder::VALIDATION_RULES);
        $preorder = Preorder::create($request->all());
        if ($preorder) {
            //init preorder created event
            event(new PreorderCreated($preorder));
        }
        return $preorder;
    }
}

