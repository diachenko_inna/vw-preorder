<?php

namespace App\Notifications;

use App\Models\Preorder;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class NewPreorders extends Notification
{
    use Queueable;

    /** @var Preorder[] $preorders */
    private $preorders;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($preorders)
    {
        $this->preorders = $preorders;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $appName = config('app.name', 'VW Preorder APP');
        $message = (new MailMessage)
            ->subject('New VW ID3 Preorder!')
            ->greeting('Hello!')
            ->line('In ' . $appName . ' were made new preorders:');

        foreach ($this->preorders as $preorder) {
            $message = $message
                ->line('Name: ' . $this->preorder->name . '.')
                ->line('Email: ' . $this->preorder->email . '.')
                ->line('Created At: ' . $this->preorder->created_at . '.')
                ->line("---");
        }

        return $message;
    }
}
