<?php

namespace App\Notifications;

use App\Models\Preorder;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class PreorderConfirmation extends Notification
{
    use Queueable;

    /** @var Preorder $preorder */
    private $preorder;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($preorder)
    {
        $this->preorder = $preorder;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $appName = config('app.name', 'VW Preorder APP');
        return (new MailMessage)
            ->subject('New VW ID3 Preorder Confirmation!')
            ->greeting('Hello, ' . $this->preorder->name . '!')
            ->line('You made preorder for Volkwagen ID3 in ' . $appName . '.')
            ->line('Your preorder is confirmed.');
    }
}
