require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

window.Vue.use(VueRouter);

import PreordersIndex from './components/preorders/Index.vue';

const routes = [
    {
        path: '/',
        components: {
            preordersIndex: PreordersIndex
        }
    }
];

const router = new VueRouter({ routes });

const app = new Vue({ router }).$mount('#app');
