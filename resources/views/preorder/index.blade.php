@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <router-view name="preordersIndex"></router-view>
                <router-view></router-view>
            </div>
        </div>
    </div>
@endsection